#ifndef __RESOURCE_H__
#define __RESOURCE_H__

struct rsc_t {
	union {
		int pid;
		struct frame_t *fm;
//		struct swapframe_t *sf;
	};
	struct rsc_t *next;
};

struct rsc_q {
	struct rsc_t *head, *tail;
};

struct rsc_t *alloc_rsc(struct rsc_q*);
void free_rsc(struct rsc_q*, struct rsc_t*);

#define malloc_rsc(x) \
	x = (struct rsc_t*) malloc (sizeof(struct rsc_t))
#endif

#include "kernel.h"
#include "proc.h"

static struct pcb_t* s_next;

inline void sched_setNext(struct pcb_t *pcb)
{
	s_next = pcb;
}

inline void scheduler()
{
	if (s_next == NULL)
		s_next = ready_q;
}

KernelContext *KCS(KernelContext *kc_in, void *current, void *next)
{
	memcpy(&(TYPE_PCB(current)->kcont), kc_in, sizeof(KernelContext));

	rewrite_kstack_pgt(TYPE_PCB(next));
	/*switch the page table for VMEM1 */
	WriteRegister(REG_PTBR1, (uint_32)(TYPE_PCB(next)->user_pg));
	/* TODO kernel stack page table */
	WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_1);

	TYPE_PCB(next)->state = PROC_RUN;

	return &(TYPE_PCB(next)->kcont);
}


extern void *kstack_cpy;
extern KernelContext *kcont_cpy;

KernelContext *
X_KCS(KernelContext *kc_in, void *current, void *next)
{
	TracePrintf(0, "abnKCS\n");

	TYPE_PCB(next)->state = PROC_RUN;
	memcpy(&(TYPE_PCB(current)->kcont), kc_in, sizeof(KernelContext));
	memcpy(&(TYPE_PCB(next)->kcont), kcont_cpy, sizeof(KernelContext));

	init_proc_kstack(next);
	rewrite_kstack_pgt(TYPE_PCB(next));

	WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_1);

	WriteRegister(REG_PTBR1, (uint_32)(TYPE_PCB(next)->user_pg));

	memcpy((char*)KERNEL_STACK_BASE, kstack_cpy, PAGESIZE * 2);
	return kcont_cpy;
}

void sched()
{
	struct pcb_t *old = current;

	/* it's possible the current_pcb is in block or wait state */

	if (PROC_RUN == current->state) {
		enqueue_ready(current);
	}

	scheduler();

	current = s_next;
	s_next = NULL;
	dequeue(&ready_q, current);
	
	if (current->state != PROC_X)
		KernelContextSwitch(KCS, (void*) old, (void*) current);
	else 

	/* 
	 * PROC_X means this proc is not forked and is the first time to run, so we need to also set its kernel context and kernel stack.
	 *
	 * HERE we copy the kernel stack directly from the current process's, and use the kc_in given by abnKCS as the kernel context.
	 * we think fork works this way
	 */
		KernelContextSwitch(X_KCS, (void*) old, (void*) current);
}


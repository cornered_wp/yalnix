#include "user.h"
#include "yalnix.h"

#define pgn	10

#define N	100

void heap_test()
{
	int i;
	char *buf;
	buf = malloc(N * 0x2000);

	if (buf == NULL) {
		Puts("malloc error\n");
		return ;
	}

	for (i = 0; i < N; i ++)
		buf[ i * PAGESIZE ] = i;

	for (i = 0; i < N; i ++)
		PutNum(buf[ i * PAGESIZE]);

	free(buf);
}

int main()
{
	char big_buffer[pgn*1024];
	int foo, i;
	char *buf;




	heap_test();

	i = Fork();
	if (0 == i) {
		Puts("Parent");
		buf = (char*) malloc (20 * PAGESIZE);
		if (buf == NULL) {
			Puts("Error");
		}
		else {
			PutNum( (int)buf );
		}
	}
	else {
		Puts("Chld");
		buf = (char*) malloc (20 * PAGESIZE);
		if (buf == NULL) {
			Puts("Error");
		}
		else {
			PutNum( (int)buf );
		}
	}

	Exit(0);
	while (1)
		;
/*
	foo = 42;
	for (i = 0; i < pgn * 1024; i++) 
	  big_buffer[i] = 'a';

	for (i = 0; i < pgn*1024; i+=512) {
	  TracePrintf(0,"&big_buffer[%d] = %x; big_buffer[%d] = %c\n",
		      i, &big_buffer[i], i, big_buffer[i]);
	}
	*/

	Exit(0);
}

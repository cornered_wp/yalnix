#include "user.h"
#include "yalnix.h"

char str[100];

int main()
{
	int pid;

	pid = Fork();
	if ( 0 == pid )
		strcpy(str, "I'm child\n");
	else
		strcpy(str, "I'm parent\n");

	Puts(str);

	while (1);
}

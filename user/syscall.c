
#include "yalnix.h"
#include "user.h"

int main()
{
	int pid = Fork();
	int i;

	if (0 == pid) {
		Exit(-1);
	}
	else {
		Wait(&i);
		PutNum(i);
	}

	while (1);
}

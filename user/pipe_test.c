
#include "yalnix.h"
#include "user.h"


void write_read() {	//write 10, read 4...totally write 100, read 40, left 60
	int i, pid;
	int pipid;
	char tmp[10];

	PipeInit(&pipid);

	pid = Fork();

	if (pid == 0) {//child
		for (i = 0; i <3; i ++) {
			PipeRead(pipid, tmp, 4);
			tmp[4] = 0;
			Puts(tmp);
		}

		Reclaim(pipid);
	}
	else {//parent
		PipeWrite(pipid, "Hello Word! ", 10);
	}

}


void test2()
{
	int pipe[10];
	char buf[10];
	int i;

	for (i = 0; i < 10; i ++) {
		PipeInit(&pipe[i]);
		PipeWrite(pipe[i], "hello\0", 6);
	}

	for (i = 0; i < 10; i ++) {
		PipeRead(pipe[i], buf, 10);
		PutNum(i);
		Puts(buf);
	}
}


void test3()
{
	char *buf;
	int pid, pipe, n, i;

	buf = (char *) malloc (PAGESIZE);

	for (i = 0; i < 1000; i ++)
		buf[i] = 'a' + (i % 26);
	buf[1000] = 0;


	PipeInit(&pipe);
	pid = Fork();

	if (pid == 0) {
		buf[10] = 0;

		for (i = 0; i < 10; i ++) {
			n = PipeRead(pipe, buf, 100);
			buf[n] = 0;
			Puts(buf);
		}

		n = PipeRead(pipe, 0, 100);
		PutNum(n);


		Exit(-1);
	}
	else {
		Delay(1);
		Puts("pipe write\n");
		for (i = 0; i < 40; i ++) {
			PipeWrite(pipe, buf, 26);
		}

	}


	Wait(&i);
	PutNum(i);
	Reclaim(pipe);
}

int main() {
	test3();

	return 0;
}


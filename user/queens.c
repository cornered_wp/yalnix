#include "yalnix.h"
#include "user.h"

char queens[13];

int ans = 0;


unsigned check_queen(int depth, int pos)
{
	int i;

	for (i = 0; i < depth; i ++)	
		if ( (queens[i] == pos) || (pos - queens[i] == i - depth) || (pos - queens[i] == depth - i) )
			return 1;
			
	return 0;

}

void solve(int depth)
{
	int i, j;
	if (depth == 8) {
		ans ++;
		for (i = 0; i < 8; i ++) {
			for (j = 0; j < 8; j ++)
				if (queens[i] == j)
					TracePrintf(0, "Q");
				else
					TracePrintf(0, "-");
			TracePrintf("\n");
		}
	}

	else {
		for (i = 0; i < 8; i ++) {
			if ( !check_queen(depth, i) ) {
				queens[depth] = i;
				solve(depth + 1);
			}
		}
	}
		
}

int main()
{
	int i, j;
	Puts("8 Queens\n");

	for (i = 0; i < 8; i ++)
		queens[i] = 0;

	solve(0);
	PutNum(ans);
}

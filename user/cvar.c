#include "yalnix.h"
#include "user.h"


#define false 0
#define true 1

int cond_odd, cond_even;
int lock;

unsigned char odd_work = false;


/*
 * if variable can share
 * but it can't (in our current implementation)
 */
void test1()
{
	int i, pid;
	CvarInit(&cond_odd);
	CvarInit(&cond_even);
	LockInit(&lock);

	pid = Fork();

	if (pid == 0) {
		for (i = 0; i < 10; i ++) {
			Acquire(lock);
			Puts("chld\n");
			while (!odd_work)
				CvarWait(cond_odd, lock);

			PutNum(i *2 + 1);
			odd_work = false;
			CvarSignal(cond_even);
			Release(lock);
		}

	}

	else {
		for (i = 0; i < 10; i ++) {
			Acquire(lock);
			Puts("parent\n");
			while (odd_work)
				CvarWait(cond_even, lock);
			PutNum(i * 2);
			odd_work = true;
			CvarSignal(cond_odd);
			Release(lock);
		}
	}


}

void test2()
{
	int pid, i;
	int cvar, lock;

	CvarInit(&cvar);
	LockInit(&lock);


	for (i = 0; i < 3; i ++) {
		pid = Fork();
		if (pid == 0)
			goto chld;
	}

	Delay(3);
	Puts("parent: broadcast\n");

	CvarSignal(cvar);

	Delay(2);
	Reclaim(lock);
	Reclaim(cvar);
	while (1);
		
chld:
	Acquire(lock);
	CvarWait(cvar, lock);
	Release(lock);
	CvarBroadcast(cvar);

	Puts("chld: cvar out!\n");
}

int main()
{
	test2();
}



#include "yalnix.h"
#include "user.h"


int lock, lock2;
int index = 0;


/* originally implemented in Custom2,
 * However now used for other purpose
 */
void Yield()
{
}

int main()
{
	int ret, pid, i;

	if ( 0 != LockInit(&lock) ) {
		Puts("Error: can't init lock\n");
		Exit(0);
	}

	pid = Fork();

	Acquire(lock);
/*
	if (pid == 0) {
		Puts("chld");
		PutNum(lock);
	}
	else {
		Puts("parent");
		PutNum(lock);
	}
	*/

	for (i = 0; i < 10; i ++) {
		if (pid == 0) {
			index++;
			Puts(" chld\n");
			Yield();
		}
		else {
			index--;
			Puts(" parent\n");
			Yield();
		}
	}

	Release(lock);
}

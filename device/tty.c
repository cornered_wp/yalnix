#include "kernel.h"
#include "proc.h"

struct term_t {
	struct pcb_t *recv_q;		/* the process waiting for the terminal*/
	int len;			/* the buf len */
	char buf[TERMINAL_MAX_LINE];	/* the buf */
};

struct pcb_t *tty_wait_q;	/* the process waiting to write to tty */
bool tty_wbusy;			/* to determine whether the tty is busy writing */

static struct term_t terms[NUM_TERMINALS];

void init_tty()
{
	int i;
	for (i = 0; i < NUM_TERMINALS; i ++) {
		terms[i].len = 0;
		terms[i].recv_q = NULL;
	}

	tty_wbusy = false;
	tty_wait_q = NULL;

}

/*
 *  when receive a line from tty, but there is no proc waiting for the line, 
 *  we discard the line (this seems to be the right action)
 *
 *  We assume tty can't be shared, otherwise this need revising
 */
void trap_tty_receive(UserContext *ucont)
{
	int i, len;
	struct pcb_t *proc;
	
	for (i = 0; i < NUM_TERMINALS; i ++) {

		/* no process is waiting for tty, discard the message */
		if (terms[i].recv_q == NULL)
			continue;

		terms[i].len = TtyReceive(i, &terms[i].buf, TERMINAL_MAX_LINE);

		if (terms[i].len > 0) {

			proc = terms[i].recv_q;
			terms[i].recv_q = proc->next;

			proc->state = PROC_READY;
			/* we let such proc run first */
			enqueue_front(&ready_q, proc);
		}
	}
}

static int trans_tid;		/* TODO not used now */
static char *trans_buf;		/* since the writing buffer is dynamic, we need free it after used */
/*
 * when a writing is finished, wake up one who is waiting to write 
 */ 
void trap_tty_transmit()
{
	struct pcb_t *proc = tty_wait_q;
	free(trans_buf);

	if (proc == NULL)
		tty_wbusy = false;

	else {
		tty_wait_q = proc->next;
		enqueue_ready(proc);
	}
}


int sys_tty_read(int id, void *buf, int len)
{
	/* block current proc if there nothing in the buf */
	if (terms[id].len == 0) {
		block_current(&terms[id].recv_q, PROC_BLOCK);
		sched();
	}

	/* copy the content to the buf */
	memcpy(buf, terms[id].buf, terms[id].len);
	((char*)buf)[terms[id].len] = 0;
	terms[id].len = 0;

	return len;
}


/* 
 * wait if tty is busy 
 */
int sys_tty_write(int id, void *buf, int len)
{
	char *sbuf = (char*) malloc (len + 1);

	
	if (sbuf == NULL)
		return 0;

	/* copy the buf to the kernel */
	memcpy(sbuf, buf, len);
	sbuf[len] = 0;

	if (tty_wbusy) {
		block_current(&tty_wait_q, PROC_BLOCK);
		sched();
	}

	tty_wbusy = true;
	trans_buf = sbuf;
	trans_tid = id;
	TtyTransmit(id, sbuf, len);


	return len;
}


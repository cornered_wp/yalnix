#include "kernel.h"
#include "proc.h"

static uint_32 ticks = 0;

void set_alarm(uint_32 units)
{
	enqueue(&delay_q, current);
	current->state = PROC_BLOCK;

	current->wake_time = ticks + units;
	sched();
}


void timer_routine()
{
	struct pcb_t *p = delay_q, *q;

	ticks ++;	
	while (p != NULL) {
		q = p->next;
		if (ticks >= p->wake_time) {
			dequeue(&delay_q, p);
			enqueue_ready(p);
		}
		p = q;
	}
}

#ifndef __PROC_H__
#define __PROC_H__

#include "kernel.h"

/*
 *  The proc state
 *  	
 *  	when create in kernel before running, 
 *  		it is in PROC_X for it need to init kstack and kcont before run for the first time
 *
 *  	proc_x ----> proc_run <---> proc_ready
 *  	proc_run -exit-> proc_zombie ---> proc_dead
 *  	proc_run <------> proc_block, proc_wait, proc_sleep
 */
#define PROC_X		0
#define PROC_RUN	1
#define PROC_READY	2
#define PROC_BLOCK	3
#define PROC_ZOMBIE	4
#define PROC_WAIT	5
#define PROC_SLEEP	6
#define PROC_DEAD	7

struct pcb_t {
	uint_8 pid;

	struct user_context ucont; // registers, trap_index, trap_code
	KernelContext kcont;
	int kcontTag;

	struct pcb_t *chld, *sibling, *parent;

	// linking in the processes queue
	// since its round robin, no priority
	struct pcb_t *next, *prev; 

	// run, ready, block, zombie, wait
	unsigned char state;

	
	struct pte kstack_pg[KERNEL_STACK_MAXSIZE >> PAGESHIFT];	/* kernel stack page table entries */
	struct pte user_pg[MAX_PT_LEN];				/* the pg_table for user land, with 128 entries */

	// vm
	struct load_info li;
	uint_8 uheap_base;
	uint_8 uheap_limit;

	//fs
	char *path;	// the path in the file system, for the unloaded situation

	//wait
	int wait_stat;
	int exit_stat;

	//delay
	uint_32 wake_time;
};
#define TYPE_PCB(x) ((struct pcb_t*) x)
#define PCB_SIZE	(sizeof(struct pcb_t))

/* page table size*/
#define PGT_SIZE	(sizeof(u_long) * MAX_PT_LEN)	


/* all kinds of queue_header */
extern struct pcb_t *ready_q, *block_q, *zombie_q, *delay_q;

#define enqueue_ready(x)	\
	enqueue(&ready_q, x); 	\
	x->state = PROC_READY


/* 	
 *	this entry is pcb page entry
 *		-2, -1 is kernel stack
 *		0 ~ 128 is user space
 */
#define get_pte(proc, index)	\
	(index < 0) ? 	\
		(&(proc->kstack_pg[2 + index])) : 	\
		(&(proc->user_pg[index]))

bool init_proc_kstack(struct pcb_t *);
void rewrite_kstack_pgt(struct pcb_t *);

int alloc_pid();
void free_pid();

#define alloc_pcb(x)	x = (struct pcb_t*) malloc (sizeof(struct pcb_t))
inline void free_pcb(struct pcb_t*);

#define PROC_KERNEL_NPG		2

#endif

/*
 * this file defines all kinds of constants used in the OS
 */


#ifndef __CONST_H__
#define __CONST_H__

#define true		1
#define false		0

#define NO_FRAME	-1
#define MAX_PID		128

#define ABORT 		-1


/* we set the timer to tick per 100ms */
#define timer_unit		100	/*in msec*/
#define sec_to_tick(x)		(x * 1000 / timer_unit)

#define  KERNEL_PGN	128

/* ERROR TYPE */

#define ERR_PRE			0x1000

/* segmentation fault */
#define ERR_SEGFAULT		(ERR_PRE | 0x1)			//4097
/* stack overflow, suppose we have max pages for stack */
#define ERR_STACKOVERFLOW	(ERR_PRE | 0x10)		//4112

#define ERR_MATH		(ERR_PRE | 0x11)

#define ERR_ILLEGAL		(ERR_PRE | 0x12)

#define ERR_DISK		(ERR_PRE | 0x13)
/* call wait, but no chld (never has a chld) */
#define ERR_NOCHLD		(ERR_PRE | 0x14)


#endif

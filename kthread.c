#include "kernel.h"
#include "proc.h"

/* 
 *	idle() and init() are two oldest kernel threads
 *	idle() does nothing, just prevent the ready queue from empty
 *	init() is responsible of opening the terminals
 *
 *	TODO swap() 
 *		may be responsible for the stuff of swap
 *		this is like the buttom half.
 *		but do we implement this?
 */
void kthread_idle()
{
	int i = 0;


	/* 
	 * free all resources used to set the kernel context 
	 * for the kernel thread.
	 * be careful, if other kernel thread is scheduled after idle
	 */
	free(kcont_cpy);
	free(kstack_cpy);

	while (1) {
//		printf("idle #%d\n", i ++);
//		TracePrintf(1, "IDLE\n");
		Pause();
	}
}

void kthread_init()
{
	int i = 0;

	while (1) {
		TracePrintf(1, "INIT\n");
		Pause();
	}

}


void kthread_swap()
{
}

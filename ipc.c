#include "kernel.h"
#include "proc.h"
#include "ipc.h"

struct ipc_t ipc_pool[MAX_IPC];

void init_ipc()
{
	int i;
	for (i = 0; i < MAX_IPC - 1; i ++)
		ipc_pool[i].type = IPC_INIT;
}

/* get the a ipc_t from the pool */
int get_ipc_id()
{
	int i;
	for(i = 0; i < MAX_IPC; i++) {
		if (ipc_pool[i].type == IPC_INIT)
			return i;
	}
	return ERROR;
}

/* 
 * check the if the id is valid and 
 * whether the corresponding ipc resource is the right type
 */
bool valid_ipc(int i, int type)
{
	if (i < 0 || i > MAX_IPC) 
		return false;

	if (ipc_pool[i].type != type)
		return false;

	return true;
}

int ipc_LockInit(int *lock_idp)
{
	int lock_id = get_ipc_id();
	struct ipc_t *p;

	if (lock_id == ERROR) 
		return ERROR;

	p = &ipc_pool[lock_id];
	*lock_idp = lock_id;

	p->type = IPC_LOCK;
	p->ipc_block_q = NULL;

	p->lock = (struct lock_t*)malloc(sizeof(struct lock_t));
	p->lock->holder = NULL;
	
	return 0;
}

int ipc_Acquire(int lock_id)
{
	/* the user provided a invalid lock id */
	if ( !valid_ipc(lock_id, IPC_LOCK) ) {
		return ERROR;
	}

	/* succeed if the current holder requires the lock again */
	if (ipc_pool[lock_id].lock->holder == current) {
		return 0;
	}

	/* other procs can't get the lock*/
	else if (ipc_pool[lock_id].lock->holder != NULL) {
		block_current(&ipc_pool[lock_id].ipc_block_q, PROC_BLOCK);
		sched();
	}
	ipc_pool[lock_id].lock->holder = current;


	return 0;
}

int ipc_Release(int lock_id)
{
	struct pcb_t *p;

	/* the user provided a invalid lock id */
	if ( !valid_ipc(lock_id, IPC_LOCK) ) {
		return ERROR;
	}

	if (ipc_pool[lock_id].lock->holder != current) {
		return ERROR;
	}
	

	p = ipc_pool[lock_id].ipc_block_q;
	if (p != NULL) {
		ipc_pool[lock_id].ipc_block_q = p->next;
		enqueue_ready(p);
	}
	ipc_pool[lock_id].lock->holder = p;

	return 0;
}

int ipc_CvarInit(int *cvar_idp)
{
	int cvar_id = get_ipc_id();
	struct ipc_t *p;
	if (cvar_id == ERROR) return ERROR;

	p = &ipc_pool[cvar_id];

	if (p == NULL)
		return ERROR;

	*cvar_idp = cvar_id;
	p->type = IPC_CVAR;
	p->ipc_block_q = NULL;
	
	return 0;
}

int ipc_CvarSignal(int cvar_id)
{
	struct pcb_t *p;

	if ( !valid_ipc(cvar_id, IPC_CVAR) )
		return ERROR;
	
	p = ipc_pool[cvar_id].ipc_block_q;

	if (p != NULL) {
		ipc_pool[cvar_id].ipc_block_q = p->next;
		enqueue_ready(p);
	}

	return 0;
}

int ipc_CvarBroadcast(int cvar_id)
{
	struct pcb_t *p; 
	if ( !valid_ipc(cvar_id, IPC_CVAR) )
		return ERROR;

	p = ipc_pool[cvar_id].ipc_block_q;

	while (p != NULL) {
		ipc_pool[cvar_id].ipc_block_q = p->next;
		enqueue_ready(p);
		p = ipc_pool[cvar_id].ipc_block_q;
	}
	return 0;
}

int ipc_CvarWait(int cvar_id, int lock_id)
{
	if ( !valid_ipc(cvar_id, IPC_CVAR) || !valid_ipc(lock_id, IPC_LOCK) )
		return ERROR;

	ipc_Release(lock_id);
	block_current(&ipc_pool[cvar_id].ipc_block_q, PROC_BLOCK);
	sched();
	ipc_Acquire(lock_id);

	return 0;
}


int ipc_Reclaim(int id)
{
	struct ipc_t *p;
	struct pipe_block *it, *tmp;

	if (id < 0 || id > MAX_IPC) 
		return ERROR;

	p = &ipc_pool[id];
	switch (p->type) {
		case IPC_LOCK:	
			if (p->lock->holder != NULL)
				return ERROR;
			if (p->ipc_block_q != NULL)
				return ERROR;
			p->type = IPC_INIT;

			free(p->lock);
			break;

		case IPC_CVAR:
			if (p->ipc_block_q != NULL)
				return ERROR;
			p->type = IPC_INIT;
			break;

#ifndef __FILE_SUPPORT__
		case IPC_PIPE:
			it = p->pipe->buf;
			while (it != NULL) {
				tmp = it;
				it = it->next;
				free(tmp);
			}
			
			p->type = IPC_INIT;
			break;
#endif

#ifdef __FILE_SUPPORT__
		case IPC_FPIPE:	/* for the file pipe */
			p->type = IPC_INIT;
			close(p->fpipe->fd);
			remove(p->fpipe->name);
			free(p->fpipe);
			break;
#endif

		case IPC_INIT:
			break;
	}

	return 0;
}


#ifndef __FILE_SUPPORT__
int ipc_PipeInit(int *fd)
{
	int id = get_ipc_id();
	struct ipc_t *ptr = &ipc_pool[id];
	struct pipe_t *pptr;

	if (id == ERROR)
		return ERROR;

	*fd = id;

	ptr->type = IPC_PIPE;
	ptr->pipe = (struct pipe_t*) malloc (sizeof(struct pipe_t));
	pptr = ptr->pipe;

	pptr->w_off = 0;
	pptr->r_off = 0;
	pptr->buf = NULL;

	return 0;
}


int ipc_PipeRead(int fd, char *buf, int len)
{
	struct pipe_t *pipe;
	
	int off, rlen, read, i;
	struct pipe_block *buf_it;

	if ( !valid_ipc(fd, IPC_PIPE) ) {
		return ERROR;
	}

	pipe = ipc_pool[fd].pipe;

	/* nothing to read */
	if ( (pipe->buf == NULL) || (pipe->r_off == pipe->w_off) ) {
		block_current(&ipc_pool[fd].ipc_block_q, current);
		sched();
	}

	/* calc the actual length of data that can be read */
	if (len < pipe->w_off - pipe->r_off)  {
		off = pipe->r_off + len; 
		rlen = len;
	}
	else {
		off = pipe->w_off;
		rlen = pipe->w_off - pipe->r_off;
	}

	/* 
	 * read data block by block, 
	 * if we read all the data from a buf block, we just free it 
	 */
	read = 0;
	i = pipe->r_off;
	buf_it = pipe->buf;
	while (PIPE_BLOCK_SIZE - i + read < rlen) {

		memcpy(&buf[read], &pipe->buf->block[i], PIPE_BLOCK_SIZE - i);
		read += PIPE_BLOCK_SIZE - i;

		i = 0;

		buf_it = buf_it->next;

		free(pipe->buf);
		pipe->buf = buf_it;

		off -= PIPE_BLOCK_SIZE;
		pipe->w_off -= PIPE_BLOCK_SIZE;
	}

	memcpy(&buf[read], &pipe->buf->block[i], rlen - read);

	/* update the records */
	if (off == pipe->w_off) {
		free(pipe->buf);
		pipe->buf = NULL;
		pipe->r_off = 0;
		pipe->w_off = 0;
	}
	else 
		pipe->r_off = off;

	return rlen;
}

int ipc_PipeWrite(int fd, char *buf, int len)
{
	struct pipe_t *pipe;
	struct pipe_block *buf_it = NULL, *pre = NULL;

	int write;
	int i, off;

	if ( !valid_ipc(fd, IPC_PIPE) )
		return ERROR;

	if (len == 0)
		return 0;

	pipe = ipc_pool[fd].pipe;
	buf_it = pipe->buf;
	off = pipe->w_off;
	
	/* find the right buf block to start writing to */
	while (off >= PIPE_BLOCK_SIZE) {
		off -= PIPE_BLOCK_SIZE;
		pre = buf_it;
		buf_it = pre->next;
	}

	if (buf_it == NULL) {
		buf_it = (struct pipe_block*) malloc ( sizeof(struct pipe_block) );

		if (pipe->buf == NULL)
			pipe->buf = buf_it;

		else
			pre->next = buf_it;
	}

	/* 
	 * write to buf block, 
	 * after fill in the current block, we prepare the next block
	 */
	write = 0;
	i = off;
	while (i + len - write> PIPE_BLOCK_SIZE) {
		memcpy(&buf_it->block[i], &buf[write], PIPE_BLOCK_SIZE - i);
		write = write + PIPE_BLOCK_SIZE - i;

		pre = buf_it;
		buf_it = (struct pipe_block*) malloc ( sizeof(struct pipe_block) );
		buf_it->next = NULL;
		pre->next = buf_it;
		i = 0;
	}

	memcpy(&buf_it->block[i], &buf[write], len - write);
	pipe->w_off += len;

	/* Ready the proc waiting to read */
	if (ipc_pool[fd].ipc_block_q != NULL) {
		enqueue_ready(ipc_pool[fd].ipc_block_q);
		ipc_pool[fd].ipc_block_q = NULL;
	}

	return len;
}
#endif


#ifdef __FILE_SPPORT__
/*
 * 	NOTICE
 *	The following is another implementation of pipe, in which filesys is used.
 *	Since writing to a file is not permited now
 *	these codes are now useless
 */
#include <fcntl.h>
#include <errno.h>
void itoa(char *buf, int i)
{
	int c = 0;
	while (i != 0) {
		buf[c++] = (i % 10) + '0';
		i = i / 10;
	}
	buf[c] = 0;
}

int ipc_pipe_init(int *pid)
{
	int id = get_ipc_id();
	struct file_pipe *pipe = (struct file_pipe*) malloc (sizeof (struct file_pipe));

	*pid = id;
	if (id == ERROR)
		return ERROR;

	ipc_pool[id].type = IPC_FPIPE;
	ipc_pool[id].fpipe = pipe;
	pipe->name[0] = '.';
	itoa(&pipe->name[1], id);
	strcat(pipe->name, "_pipe");

	pipe->fd = open(pipe->name, O_RDWR | O_CREAT);
	if (pipe->fd < 0) {
		printf("[PIPE] Can't open file %s, error: %d\n", pipe->name, errno);
		return ERROR;
	}
	
	pipe->seek_cur = 0;
	pipe->seek_end = 0;

	return 0;
}

int ipc_pipe_read(int id, char *buf, int len)
{
	struct file_pipe *pipe = ipc_pool[id].fpipe;
	int rlen;

	if ( !valid_ipc(id, IPC_PIPE) )
		return ERROR;

	if (pipe->seek_cur == pipe->seek_end)
		return ERROR;

	if (len == 0)
		return 0;


	/* nothing to read */
	if (pipe->seek_cur == pipe->seek_end) {
		block_current(&ipc_pool[id].ipc_block_q, PROC_BLOCK);
		sched();
	}
		
	if (pipe->seek_cur + len <= pipe->seek_end)
		rlen = len;
	else
		rlen = pipe->seek_end - pipe->seek_cur;

	lseek(pipe->fd, pipe->seek_cur, SEEK_SET);
	read(pipe->fd, buf, rlen);
	pipe->seek_cur += rlen;

	return rlen;
}

int ipc_pipe_write(int id, char *buf, int len)
{
	struct file_pipe *pipe = ipc_pool[id].fpipe;
	struct pcb_t *proc;
	int wlen;

	if ( !valid_ipc(id, IPC_FPIPE) )
		return ERROR;
	if (len == 0)
		return 0;

	lseek(pipe->fd, pipe->seek_end, SEEK_SET);

	wlen = write(pipe->fd, buf, len);
	pipe->seek_end += len;

	proc = ipc_pool[id].ipc_block_q;
	if (proc != NULL) {
		enqueue_ready(proc);
		ipc_pool[id].ipc_block_q = NULL;
	}

	return len;
}
#endif

#ifndef __KERNEL_H__
#define __KERNEL_H__


#include <hardware.h>
#include <yalnix.h>
#include <load_info.h>
#include "types.h"
#include "const.h"
#include "mm.h"


#define DEBUG

extern uint_32 PMEM_LIMIT;		//physical memory size
extern void *bss_base, *bss_limit;	//kernel data base, data limit

extern struct pte kernel_pgt[];


extern int pmem_fn;

extern uint_32 KERNEL_BSS_BASE, KERNEL_BSS_LIMIT; 
extern uint_32 KERNEL_STK_BASE;
extern uint_32 KERNEL_HEAP_BASE, KERNEL_HEAP_LIMIT;

extern uint_32 KERNEL_BSS_PGB, KERNEL_BSS_PGL;
extern uint_32 KERNEL_HEAP_PGB, KERNEL_HEAP_PGL;
extern uint_32 KERNEL_STACK_PGB, KERNEL_STACK_PGL;
#define USER_STACK_MAXNPG	2

extern uint_32 PMEM_LIMIT;

extern KernelContext *kcont_cpy;
extern void *kstack_cpy;
extern UserContext ucont_cpy;

extern struct pcb_t *current;
#endif

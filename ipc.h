#ifndef __IPC_H__
#define __IPC_H__

#define MAX_IPC		100

/* ipc types */
#define IPC_LOCK	1
#define IPC_CVAR	2
#define IPC_PIPE	3
#define IPC_FPIPE	4
#define IPC_INIT	0


/*
 * 	TODO
 *	A problem need to consider:
 *	what if a program loops over all possible ipc id
 *	and call reclaim for each of them.
 *	So we should maintain a list of users who could relaim the ipc
 *		then when we fork, we should also update the ipc structure.
 *		neccessary and expensive.
 *
 *	haven't implemented our current version.
 */

struct ipc_t {
	int type;
	union {
		struct lock_t *lock;
		struct pipe_t *pipe;
		struct file_pipe *fpipe;
	};	
	struct pcb_t *ipc_block_q;
};

struct lock_t {
	struct pcb_t *holder;
};

#define PIPE_BLOCK_SIZE 	64
struct pipe_block {
	char block[PIPE_BLOCK_SIZE];
	struct pipe_block *next;
};

struct pipe_t {
	struct pipe_block *buf;		
	/* the buffer for pipe, it's a dynamic linked structure, so the buffer size will increase or decrease according to demand */
	
	int w_off;
	int r_off;
};

/* pipe by file */
struct file_pipe {
	/* read fd, write fd */
	int fd;
	int seek_cur, seek_end;
	char name[10];
};

/*
struct cvar_t {
	struct pcb_t block_q; // actually don't need anything now 
};
*/

#endif

#include "kernel.h"
#include "proc.h"
#include <fcntl.h>

void trap_kernel(UserContext*);
void trap_clock(UserContext*);
void trap_illegal(UserContext*);
void trap_memory(UserContext*);
void trap_math(UserContext*);
void trap_tty_receive(UserContext*);
void trap_tty_transmit(UserContext*);
void trap_disk(UserContext*);


/* int */
typedef void (*trap_handler) (UserContext *);
trap_handler int_vector[TRAP_VECTOR_SIZE] = {
	&trap_kernel,			/* 0 */
	&trap_clock,
	&trap_illegal,			/* 2 */
	&trap_memory,
	&trap_math,			/* 4 */
	&trap_tty_receive,
	&trap_tty_transmit,
	&trap_disk,			/* 7 */
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL,
	NULL				/* 15 */
};

inline bool check_uaddr(uint_32 addr)
{
	if (addr < (MAX_PT_LEN << PAGESHIFT))
		return false;
	return true;
}

void trap_kernel(UserContext *ucont)
{
	int fd;

	TracePrintf(0, "[TRAP KERNEL], proc#%d\n", current->pid);

	switch ( ucont->code ) {
		case YALNIX_GETPID:
			ucont->regs[0] = current->pid;
			break;

		case YALNIX_FORK:
			TracePrintf(0, " [Fork]\n");
			ucont->regs[0] = sys_fork(ucont);
			break;

		case YALNIX_EXEC:
			TracePrintf(0, " [Exec]\n");
			sys_exec((char*)ucont->regs[0], (char**)ucont->regs[1]);
			memcpy(ucont, &current->ucont, sizeof(UserContext));

			break;

		case YALNIX_DELAY:
			TracePrintf(0, " [Delay]\n");
			sys_delay(sec_to_tick(ucont->regs[0]));
			//sys_delay(ucont->regs[0]);
			break;

		case YALNIX_BRK: 
			TracePrintf(0, " [Brk]\n");
			ucont->regs[0] = sys_brk(ucont->regs[0]);

			if (ucont->regs[0])
				TracePrintf(0, "[Brk] Success! new heap limit: %d\n", current->uheap_limit);
			else
				TracePrintf(0, "[Brk] Failed!\n");

			break;

		case YALNIX_EXIT:
			TracePrintf(0, " [Exit]\n");
			sys_exit(ucont->regs[0]);
			break;

		case YALNIX_WAIT:
			TracePrintf(0, " [Wait] pid: %d\n");

			if (check_uaddr(ucont->regs[0]))
				sys_wait(ucont->regs[0]);
			else
				ucont->regs[0] = -1;

			TracePrintf(0, " [Wait return] pid: %d\n");
			break;
	
		case YALNIX_TTY_READ:
			TracePrintf(0, " [TtyRead] pid: %d\n");

			if (check_uaddr(ucont->regs[1]))
				ucont->regs[0] = sys_tty_read(ucont->regs[0], ucont->regs[1], ucont->regs[2]);
			else
				ucont->regs[0] = -1;
			break;
			
		case YALNIX_TTY_WRITE:
			TracePrintf(0, " [TtyWrite] pid: %d\n");
			if (check_uaddr(ucont->regs[1]))
				ucont->regs[0] = sys_tty_write(ucont->regs[0], ucont->regs[1], ucont->regs[2]);
			else
				ucont->regs[0] = -1;
			break;

		case YALNIX_RECLAIM:
			/* ipc id */
			ipc_Reclaim(ucont->regs[0]);	
			break;


		case YALNIX_LOCK_INIT:

			if (check_uaddr(ucont->regs[0]))
				ucont->regs[0] = ipc_LockInit(ucont->regs[0]);
			else
				ucont->regs[0] = -1;

			break;

		case YALNIX_LOCK_ACQUIRE:
			ucont->regs[0] = ipc_Acquire(ucont->regs[0]);
			break;

		
		case YALNIX_LOCK_RELEASE:
			ucont->regs[0] = ipc_Release(ucont->regs[0]);
			break;

		case YALNIX_CVAR_INIT:

			if (check_uaddr(ucont->regs[0]))
				ucont->regs[0] = ipc_CvarInit(ucont->regs[0]);
			else
				ucont->regs[0] = -1;

			break;

		case YALNIX_CVAR_SIGNAL:
			ucont->regs[0] = ipc_CvarSignal(ucont->regs[0]);
			break;

		case YALNIX_CVAR_BROADCAST:
			ucont->regs[0] = ipc_CvarBroadcast(ucont->regs[0]);
			break;

		case YALNIX_CVAR_WAIT:
			ucont->regs[0] = ipc_CvarWait(ucont->regs[0], ucont->regs[1]);
			break;

		case YALNIX_PIPE_INIT:
			if (check_uaddr(ucont->regs[0]))
				ucont->regs[0] = ipc_PipeInit(ucont->regs[0]);
			else
				ucont->regs[0] = -1;
			//ucont->regs[0] = ipc_pipe_init(ucont->regs[0]);
			break;

		case YALNIX_PIPE_READ:
			if (check_uaddr(ucont->regs[1]))
				ucont->regs[0] = ipc_PipeRead(ucont->regs[0], ucont->regs[1], ucont->regs[2]);
			else
				ucont->regs[0] = -1;
			//ucont->regs[0] = ipc_pipe_read(ucont->regs[0], ucont->regs[1], ucont->regs[2]);
			break;

		case YALNIX_PIPE_WRITE:
			if (check_uaddr(ucont->regs[1]))
				ucont->regs[0] = ipc_PipeWrite(ucont->regs[0], ucont->regs[1], ucont->regs[2]);
			else
				ucont->regs[0] = -1;
			//ucont->regs[0] = ipc_pipe_write(ucont->regs[0], ucont->regs[1], ucont->regs[2]);
			break;


		/* debug */
		case YALNIX_CUSTOM_0:
			printf("PROC#%d: %s\n", current->pid, ucont->regs[0]);
			break;

		case YALNIX_CUSTOM_1:
			printf("Pid: %d says: %d\n", current->pid, ucont->regs[0]);
			break;

		/* provide a more precise timer control */
		case YALNIX_CUSTOM_2:
			sys_delay(ucont->regs[0]);
			break;
			
		default:
			TracePrintf(0, " [syscall%x] pid: %d\n", ucont->code);
	}
}


void trap_memory(UserContext *ucont)
{	

	uint_32 addr = (uint_32)ucont->addr;
	int page_id = addr >> PAGESHIFT, fid, i;
	struct pte *ptr;

	TracePrintf(0, "trap_memory\n");
	/* 
	 * error occurred in kernel page table
	 * swapped out ?
	 */
	if (page_id < 126)
		ptr = &kernel_pgt[page_id];

	/* kernel stack */
	else if (page_id < 128)
		ptr = &(current->kstack_pg[page_id - 126]);
	else
		ptr = &(current->user_pg[page_id - 128]);
		

	fid = ptr->pfn;
	TracePrintf(0, " Page (%d): val %d, prot %d, frame %d\n", page_id, ptr->valid, ptr->prot, ptr->pfn);


	if ( !ptr->valid ) {
		// do we have a user stack maxsize?
		if (page_id >= MAX_PT_LEN * 2 - USER_STACK_MAXNPG) {
			
			TracePrintf(0, " max pt: %d, ustack max: %d\n", MAX_PT_LEN, USER_STACK_MAXNPG);
			alloc_upage(current, page_id - 128, PROT_READ | PROT_WRITE);
		}
		/*
		else if (check_page_unload(ptr)) {
		else if (check_page_swap(ptr)) {
		*/
		else 
			sys_exit(ERR_STACKOVERFLOW);
	}
	else {

		if (check_ccow(page_id))
			fork_page(page_id, ptr);
		else {
			TracePrintf(0, "Segmentation Fault\n");
			sys_exit(ERR_SEGFAULT); //seg fault
		}

	}

	return ;
}

void trap_clock(UserContext *ucont)
{

	timer_routine(ucont); //check the delayed procs
	sched();
}

void trap_math(UserContext *ucont)
{
	sys_exit(ERR_MATH);
}

void trap_illegal(UserContext *ucont)
{
	sys_exit(ERR_ILLEGAL);
}

void trap_disk(UserContext *ucont)
{
	sys_exit(ABORT);
}


inline void init_trap()
{
	WriteRegister(REG_VECTOR_BASE, (uint_32)int_vector);
}

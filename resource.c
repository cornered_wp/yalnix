#include "kernel.h"
#include "resource.h"


/* get a resource from the free resource queue */
inline struct rsc_t* alloc_rsc(struct rsc_q *queue)
{
	struct rsc_t *p = queue->head;
	if (queue->head != NULL) {
		queue->head = p->next;
		if (queue->head == NULL)
			queue->tail = NULL;
	}

	return p;
}


/* put the resource to the free resource queue */
inline void free_rsc(struct rsc_q *queue, struct rsc_t *rsc)
{
	if (queue->tail == NULL) {
		queue->head = rsc;
		queue->tail = rsc;
	}
	else {
		queue->tail->next = rsc;
		queue->tail = rsc;
	}
}

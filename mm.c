/*
 * mm module includes basic functions for paging and frame
 *
 * we are going to implement copy-on-write
 *	and if possible demand paging 
 *
 */


#include <assert.h>
#include "kernel.h"
#include "proc.h"
#include "resource.h"

struct rsc_q free_frames;
struct rsc_t *frames;
#define frame(x) (frames[x].fm)

int pmem_fn;	/* Physical memory frame number (512), this variable is set in KernelStart */
int ff_cnt;	/* free frames counter */


void init_pmem()
{
	int i = 0;
	struct rsc_t *p;

	frames = (struct rsc_t*) malloc (sizeof(struct rsc_t) * pmem_fn);

	if (frames == NULL)
		TracePrintf(0, "ERROR when alloc\n");

	for (; i < pmem_fn; i ++) {
		frame(i) = (struct frame_t*)malloc(sizeof(struct frame_t));
		if (frame(i) == NULL) {
			TracePrintf(0, "Error\n");
			break;
		}
		
		frame(i)->fid = i;

		if (i < KERNEL_PGN)
			frame(i)->valid = false;
		else
			frame(i)->valid = true;

		if (i == pmem_fn - 1)
			frames[i].next = NULL;
		else
			frames[i].next = &frames[i+1];

	}

	/* later we should change the 128 to a varible or macro */
	ff_cnt = pmem_fn - KERNEL_PGN;

	free_frames.head = &frames[KERNEL_PGN];
	free_frames.tail = &frames[pmem_fn-1];
}



/* 
 * because copy_on_write, a frame could be shared by different pages
 * if a frame is swapped, we the the holders information to modify the page table entry
 *
 * frame_add_holder, and frame_remove_holder implement the functionality
 *
 * this index is page_id - 128
 */
inline bool frame_add_holder(struct pcb_t *proc, int index, fid_t fid)
{
	struct frame_holder *fh = (struct frame_holder*) malloc (sizeof(struct frame_holder));

	if (fh == NULL)
		return false;

	fh->proc = proc;
	fh->index = index;
	fh->next = frame(fid)->holder_list;
	frame(fid)->holder_list = fh;

	return true;
}

struct frame_holder *frame_remove_holder(struct pcb_t *proc, int index, fid_t fid)
{
	struct frame_holder *pre = NULL, *it = frame(fid)->holder_list;

	while (it != NULL) {
		if ((proc->pid == it->proc->pid) && (index == it->index))
			break;

		pre = it;
		it = it->next;
	}

	if (it == NULL)
		return NULL;

	if (pre == NULL)
		frame(fid)->holder_list = it->next;
	else
		pre->next = it->next;

	return it;
}



/* alloc_frame & free_frame */
fid_t alloc_frame(struct pcb_t *proc, int index)
{

	/* 
	 * If there is a free frame, return fid of the free frame.
	 */
	struct rsc_t *rsc = alloc_rsc(&free_frames);
	struct pte *pptr = get_pte(proc, index);
	fid_t fid = NO_FRAME;

	if (rsc != NULL) {
		fid = (rsc->fm)->fid;
		frame(fid)->holder_list = NULL;

		if (!frame_add_holder(proc, index, fid)) {
			free_rsc(&free_frames, rsc);
			return NO_FRAME;
		}

		frame(fid)->valid = false;
		frame(fid)->ref = 1;
	}



	ff_cnt --;
	return fid;
}

fid_t fh_alloc_frame(struct frame_holder *fh)
{
	struct rsc_t *rsc = alloc_rsc(&free_frames);
	struct pte *pptr = get_pte(fh->proc, fh->index);

	fid_t fid = NO_FRAME;

	if (rsc != NULL) {
		fid = (rsc->fm)->fid;
		fh->next = frame(fid)->holder_list;
		frame(fid)->holder_list = fh;

		frame(fid)->valid = false;
		frame(fid)->ref = 1;
		frame(fid)->prot = pptr->prot;
	}

	ff_cnt --;
	return fid;

}

inline void free_frame(fid_t fid)
{
	free_rsc(&free_frames, &frames[fid]);
	frame(fid)->valid = true;
	ff_cnt ++;
}


/*
 *	this function discards a page
 *	this index is from -2 to MAX_PT_LEN
 */
void discard_page(struct pcb_t *proc, int index)
{
	fid_t fid;
	struct frame_holder *it;
	struct pte *pptr = get_pte(proc, index);

	/* no need to reclaim any frame or swap frame */
	/*
	if (!pptr->valid && !check_page_swap(pptr))
		return ;
	*/

	if (pptr->valid) {	/* reclaim the frame */
		fid = pptr->pfn;
		frame(fid)->ref --;
		free( frame_remove_holder(proc, index, fid) );

		if (frame(fid)->ref == 0)
			free_frame(fid);	

		else if (frame(fid)->ref == 1) {
			it = frame(fid)->holder_list;
			it->proc->user_pg[index].prot = frame(fid)->prot;
		}
	}

	/*
	 * Didnot have time to implement the swap part
	 
	else {	
		the swap area
	}

 	*/
}


/*
 * if a frame is shared by pages because of fork, 
 * we forbid writing by unsetting the PROT_WRITE bit.
 * 
 * so when a trap_memory raises, we need to check the origin 
 *
 * this page_id include the kernel page
 */
bool check_cow(struct pcb_t *proc, int page_id)
{
	struct pte *pptr = &proc->user_pg[page_id - KERNEL_PGN];

	/* 
	 * we dont share kernel stack 
	 * so if page_id < data_pg1, not possible for cow
	 */
	if ( (page_id << PAGESHIFT) < proc->li.id_vaddr ) {
		TracePrintf(0, "check cow false\n");
		return false;
	}

	if (!pptr->valid) {
		TracePrintf(0, "check cow: pptr now valid\n");
		return false;
	}

	if (check_prot_w(frame(pptr->pfn)->prot) && 
		!check_prot_w(pptr->prot)) 
		return true;

	return false;
}


inline bool alloc_upage(struct pcb_t *proc, int upage_id, uint_8 prot)
{
	proc->user_pg[upage_id].pfn = alloc_frame(proc, upage_id);
	if (proc->user_pg[upage_id].pfn != NO_FRAME) {
		proc->user_pg[upage_id].valid = true;
		proc->user_pg[upage_id].prot = prot;
		return true;
	}
	return false;
}


/* 
 * copy the user page table from parent_pg to chld_pg
 */
void dup_pgt(struct pcb_t *chld, struct pcb_t *parent)
{
	int i, prot;
	fid_t fid;

	for (i = 0; i < MAX_PT_LEN; i ++) {
		if (!parent->user_pg[i].valid)
			continue;

		prot = parent->user_pg[i].prot;
		fid = parent->user_pg[i].pfn;

		if (frame(fid)->ref == 1) {
			frame(fid)->prot = parent->user_pg[i].prot;
			if (check_prot_w(prot))
				parent->user_pg[i].prot -= PROT_WRITE;
		}

		frame(fid)->ref ++;
		frame_add_holder(chld, i, fid);

	}
			
	memcpy(chld->user_pg, parent->user_pg, PGT_SIZE);
}

/* 
 *
 * Args: vmem page table entry, 
 * Function: copy the contents of the page to a new frame,
 * 	and modify the page table entry
 *		with the shadow frame table
 *	to implement copy_on_write
 *
 *
 * TODO
 * 	if we use the same structure for frame 
 * 	in both memory and swap, 
 * 	then this function can be reused
 */
int fork_page(int page_id, struct pte *pptr)
{

	struct frame_t* fptr = frame(pptr->pfn);
	struct frame_holder *fh;
	fid_t fid;
	char *buf, *addr = (char*) (page_id << PAGESHIFT);
	int index = page_id - KERNEL_PGN;


	assert(fptr->ref > 1);

	fh = frame_remove_holder(current, index, pptr->pfn);
	assert(fh != NULL);

	buf = (char*) malloc (PAGESIZE);

	if (buf == NULL)
		return ERROR;

	memcpy(buf, addr, PAGESIZE);
	fid = fh_alloc_frame(fh);
	current->user_pg[index].pfn = fid;
	current->user_pg[index].prot = fptr->prot;
	TLB_FLUSH(addr);
	memcpy(addr, buf, PAGESIZE);
	free(buf);

 	fptr->ref --;
 	if (fptr->ref == 1) {
		fh = fptr->holder_list;
		fptr->holder_list->proc->user_pg[index].prot = fptr->prot;
	}

	return 0;
}

/*
 * swap_frame swaps a frame out to the disk, 
 * when there is no free frame left.
 * 
 * DO NOT NEED TO IMPLEMENT THIS NOW.
 */
fid_t swap_frame()
{

/* basic idea:
	read only 
	writable but clean

	not recently used ?
	if the process is blocked, it might be good (contraversial still)
 */

	fid_t fid = NO_PAGE;

	/* find a frame */

	/* write it to the disk */

	/* record the information to the page table of the pcb */

	return fid; 
	/* when 0 is returned, it means that there is no swap area! Block the process to the swap_queue */
}


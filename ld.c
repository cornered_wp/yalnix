
#include <fcntl.h>
#include <unistd.h>
#include <hardware.h>
#include <load_info.h>

#include "kernel.h"
#include "proc.h"



int LoadProgram(char *name, char *args[], struct pcb_t *proc)
{
	int fd;
	int (*entry)();
	struct load_info li;
	int i;
	char *cp;
	char **cpp;
	char *cp2;
	int argcount;
	int size;
	int text_pg1;
	int data_pg1;
	int data_npg;
	int stack_npg;
	long segment_size;
	char *argbuf;

	/*
	* Open the executable file 
	*/
	TracePrintf(0, "Load Program %s \n", name);
	if ((fd = open(name, O_RDONLY)) < 0) {
		TracePrintf(0, "LoadProgram: can't open file '%s'\n", name);
		return ERROR;
	}

	if (LoadInfo(fd, &li) != LI_NO_ERROR) {
		TracePrintf(0, "LoadProgram: '%s' not in Yalnix format\n", name);
		close(fd);
		return (-1);
	}

	if (li.entry < VMEM_1_BASE) {
		TracePrintf(0, "LoadProgram: '%s' not linked for Yalnix\n", name);
		close(fd);
		return ERROR;
	}

	text_pg1 = (li.t_vaddr - VMEM_1_BASE) >> PAGESHIFT;
	data_pg1 = (li.id_vaddr - VMEM_1_BASE) >> PAGESHIFT;
	data_npg = li.id_npg + li.ud_npg;

	//printf("text area: %d npg: %d; data area: %d, npg: %d \n" , text_pg1, li.t_npg, data_pg1, data_npg);

	size = 0;
	for (i = 0; args[i] != NULL; i++) {
		TracePrintf(3, "counting arg %d = '%s'\n", i, args[i]);
		size += strlen(args[i]) + 1;
	}
	argcount = i;


	TracePrintf(2, "LoadProgram: argsize %d, argcount %d\n", size, argcount);

	/*
	*  The arguments will get copied starting at "cp", and the argv
	*  pointers to the arguments (and the argc value) will get built
	*  starting at "cpp".  The value for "cpp" is computed by subtracting
	*  off space for the number of arguments (plus 3, for the argc value,
	*  a NULL pointer terminating the argv pointers, and a NULL pointer
	*  terminating the envp pointers) times the size of each,
	*  and then rounding the value *down* to a double-word boundary.
	*/
	cp = ((char *)VMEM_1_LIMIT) - size;

	cpp = (char **)
		(((int)cp - 
		((argcount + 3 + POST_ARGV_NULL_SPACE) *sizeof (void *))) 
		& ~7);

	/*
	* Compute the new stack pointer, leaving INITIAL_STACK_FRAME_SIZE bytes
	* reserved above the stack pointer, before the arguments.
	*/
	cp2 = (caddr_t)cpp - INITIAL_STACK_FRAME_SIZE;

	TracePrintf(1, "prog_size %d, text %d data %d bss %d pages\n",
	      li.t_npg + data_npg, li.t_npg, li.id_npg, li.ud_npg);


	/* 
	* Compute how many pages we need for the stack */
	stack_npg = (VMEM_1_LIMIT - DOWN_TO_PAGE(cp2)) >> PAGESHIFT;

	TracePrintf(1, "LoadProgram: heap_size %d, stack_size %d\n",
	      li.t_npg + data_npg, stack_npg);


	/* leave at least one page between heap and stack */
	if (stack_npg + data_pg1 + data_npg >= MAX_PT_LEN) {
		close(fd);
		return ERROR;
	}

	/*
	* This completes all the checks before we proceed to actually load
	* the new program.  From this point on, we are committed to either
	* loading succesfully or killing the process.
	*/


	/*
	* Set the new stack pointer value in the process's exception frame.
	*/
	proc->ucont.sp = cp2;
	proc->ucont.ebp = (void*) (VMEM_1_LIMIT - 4);

	/*
	* Now save the arguments in a separate buffer in region 0, since
	* we are about to blow away all of region 1.
	*/
	cp2 = argbuf = (char *) malloc (size);
	for (i = 0; args[i] != NULL; i ++) {
		TracePrintf(3, "saving arg %d = '%s'\n", i, args[i]);
		strcpy(cp2, args[i]);
		cp2 += strlen(cp2) + 1;
	}

	/*
	* Set up the page tables for the process so that we can read the
	* program into memory.  Get the right number of physical pages
	* allocated, and set them all to writable.
	*/

	/*
	 * PEI:
	 * Actually we need to be careful about context switch
	 * But it seems that we can do these even in userland
	 */


	for (i = 0; i < MAX_PT_LEN; i ++)
		discard_page(proc, i);
		
	/* TEXT first read and write, dont forget to set readonly */
	for (i = text_pg1; i < text_pg1 + li.t_npg; i ++)
		alloc_upage(proc, i, PROT_READ | PROT_WRITE);

	/* BSS */
	for (i = data_pg1; i < data_pg1 + data_npg; i ++) 
		alloc_upage(proc, i, PROT_READ | PROT_WRITE);
	
	/* User HEAP */
	proc->uheap_base = i;
	proc->uheap_limit = i;

	/* U STACK */
	i = MAX_PT_LEN - stack_npg;
	for (; i < MAX_PT_LEN; i ++)
		alloc_upage(proc, i, PROT_READ | PROT_WRITE);


	lseek(fd, li.t_faddr, SEEK_SET);
	segment_size = li.t_npg << PAGESHIFT;

	WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_1);
	if (read(fd, (void *)li.t_vaddr, segment_size) != segment_size) {
		TracePrintf(0, "error\n");
		close(fd);
		//sys_exit(-1);
		return ERROR;
	}

	lseek(fd, li.id_faddr, 0);
	segment_size = li.id_npg << PAGESHIFT;
	if (read(fd, (void *)li.id_vaddr, segment_size) != segment_size) {
		TracePrintf(0, "error\n");
		close(fd);
//		sys_exit(-1);
		return ERROR;
	}

	/* reset TEXT prot */
	for (i = text_pg1; i < text_pg1 + li.t_npg; i ++) {
		proc->user_pg[i].prot = PROT_READ | PROT_EXEC;
		WriteRegister(REG_TLB_FLUSH, (i + 128) << PAGESHIFT);
	}
	
	close(fd);

	bzero(li.id_end, li.ud_end - li.id_end);
	proc->ucont.pc = (void*)li.entry;

	memcpy(&proc->li, &li, sizeof(struct load_info));

	
#ifdef LINUX
	memset(cpp, 0x00, VMEM_1_LIMIT - ((int) cpp));
#endif

	*cpp++ = (char *)argcount;		/* the first value at cpp is argc */
	cp2 = argbuf;
	for (i = 0; i < argcount; i++) {      /* copy each argument and set argv */
		*cpp++ = cp;
		strcpy(cp, cp2);
		cp += strlen(cp) + 1;
		cp2 += strlen(cp2) + 1;
	}
	free(argbuf);
	*cpp++ = NULL;			/* the last argv is a NULL pointer */
	*cpp++ = NULL;			/* a NULL pointer for an empty envp */

	return 0;
}

#ifndef __PMEM_H__
#define __PMEM_H__

#include "types.h"

/* 
 * Pei: Change the name as you like.
 * Just to emphasize the frame struct needs to know which page it binds to 
 */

struct frame_holder {
	struct pcb_t *proc;
	int index;	/* this index contains the kstack, so from -2 to MAX_PT_LEN*/
	struct frame_holder *next;
};

struct frame_t {
	struct frame_holder* holder_list;	/*before writing, the frame could be shared */
	fid_t fid;
	uint_8 ref;				/* counter of sharing */
	uint_8 valid	:1;
	uint_8 prot	:4;
	uint_8 		:3;
};
#define NO_PAGE 	(-1)

#define	TLB_FLUSH(addr)	\
	WriteRegister(REG_TLB_FLUSH, (uint_32)addr);


bool check_cow(struct pcb_t*, int page_id);
#define check_ccow(page_gid)	\
	check_cow(current, page_gid)

/* a counter for the free frames */
extern ff_cnt;



/* 
 * TODO the following are for DEMAND PAGING and SWAP
 * 		but neither is implemented
 */

/* macros for check the state bits in page table*/
#define PAGE_UNLOAD	(1 << 27)		// set bit if page unloaded
#define PAGE_SWAP	(1 << 26)		// set bit if page swapped out
#define PAGE_MASK	~(3 << 26)

#define unset_page(ptr, p)	\
	p = (u_long)ptr;	\
	*p = *p & PAGE_MASK

#define set_page_unload(ptr, p)	\
	p = (u_long)ptr;	\
	*p = *p | PAGE_UNLOAD

#define check_page_unload(ptr)	\
	((u_long)*ptr & PAGE_UNLOAD)

#define set_page_swap(ptr, p)	\
	p = (u_long)ptr;	\
	*p = *p | PAGE_SWAP

#define check_page_swap(ptr)	\
	((u_long)*ptr & PAGE_SWAP)

#define check_prot_w(prot)	\
	(prot & PROT_WRITE)

#define check_prot_r(prot)	\
	(prot & PROT_READ)

#define check_prot_x(prot)	\
	(prot & PROT_EXEC)

#endif

/*
 * this file defines kinds of types used in the OS
 */

#ifndef __TYPES_H__
#define __TYPES_H__

#define bool		unsigned char
#define uint_8		unsigned char
#define uint_32		unsigned int

typedef int fid_t;	/* type for frame id,  you can use other type like char or anything*/


#endif

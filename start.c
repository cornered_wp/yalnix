#include "kernel.h"
#include "proc.h"
#include <fcntl.h>


uint_32 KERNEL_BSS_BASE, KERNEL_BSS_LIMIT; 
uint_32 KERNEL_STK_BASE;
uint_32 KERNEL_HEAP_BASE, KERNEL_HEAP_LIMIT;

uint_32	KERNEL_BSS_PGB, KERNEL_BSS_PGL;
uint_32	KERNEL_HEAP_PGB, KERNEL_HEAP_PGL;
uint_32	KERNEL_STACK_PGB, KERNEL_STACK_PGL;

uint_32	PMEM_LIMIT;


/* 
 * these three variables are used for the system processes ( kernel threads )
 * they accomplished their duty after all kernel threads scheduled
 */
KernelContext *kcont_cpy;
void *kstack_cpy;
UserContext ucont_cpy;

struct pte kernel_pgt[MAX_PT_LEN];

char **init_args;


void SetKernelData(void *_KernelDataStart, void *_KernelDataEnd)
{
	KERNEL_BSS_BASE = (uint_32)_KernelDataStart;
	KERNEL_BSS_LIMIT = (uint_32)_KernelDataEnd;
}


/* 
 * To init the kernel page table 
 *	1. the kernel vmem should mapped to the correspondent pmem
 *	2. the text area are executable, but not writable
 *	3. the bss area are readable and writable
 *	4. the heap area have only one frame at beginning
 *	5. we can actually calculate the stack, and alloc the frame
 */
void init_kernel_pgt()
{
	int i;

	/* init kernel page table */
	uint_32 bssl = UP_TO_PAGE(KERNEL_BSS_LIMIT);
	uint_32 bssb = DOWN_TO_PAGE(KERNEL_BSS_BASE);
	KERNEL_BSS_PGL = bssl >> PAGESHIFT;
	KERNEL_BSS_PGB = bssb >> PAGESHIFT;

	/* page table */
	for (i = 0; i < MAX_PT_LEN; i ++) {
		kernel_pgt[i].pfn = i;
		kernel_pgt[i].valid = false;
		kernel_pgt[i].prot = PROT_READ | PROT_WRITE;
	}

	/* text area */
	for (i = 0; i < KERNEL_BSS_PGB; i ++) {
		kernel_pgt[i].valid = true;
		kernel_pgt[i].prot = PROT_READ | PROT_EXEC;	
	}

	/* bss area */
	for (; i < KERNEL_BSS_PGL; i ++) {
		kernel_pgt[i].valid = true;
		kernel_pgt[i].prot = PROT_WRITE | PROT_READ;	
	}

	/* HEAP AREA only one frame at first*/
	KERNEL_HEAP_PGB = KERNEL_BSS_PGL;
	KERNEL_HEAP_PGL = KERNEL_HEAP_PGB + 1;
	kernel_pgt[i].valid = true;
	kernel_pgt[i].prot = PROT_READ | PROT_WRITE;			


	/* STACK AREA */
	KERNEL_STK_BASE = DOWN_TO_PAGE(KERNEL_STACK_LIMIT -  PAGESIZE);
	KERNEL_STACK_PGB = KERNEL_STACK_BASE >> PAGESHIFT;
	KERNEL_STACK_PGL = UP_TO_PAGE(KERNEL_STACK_LIMIT) >> PAGESHIFT;
	for (i = KERNEL_STACK_PGB; i < KERNEL_STACK_PGL; i ++) {
		kernel_pgt[i].valid = true;
		kernel_pgt[i].prot = PROT_READ | PROT_WRITE;
		kernel_pgt[i].pfn = i;
	}

}


/* Use KernelContextSwitch() to get a kernel context */
KernelContext *get_kcont(KernelContext *kc_in, void *pt1, void *pt2)
{
	void *kstack_addr = (void*) (VMEM_0_LIMIT - 2 * PAGESIZE);

	kcont_cpy = (KernelContext*) malloc (sizeof(KernelContext));
	kstack_cpy = (void*) malloc (PAGESIZE * 2);

	memcpy(kcont_cpy, kc_in, sizeof(KernelContext));
	memcpy(kstack_cpy, kstack_addr, PAGESIZE * 2);

	return kc_in;
}
#define init_kcont()	\
	KernelContextSwitch(get_kcont, NULL, NULL)

/* the start point of the kernel */
void KernelStart
	(char *cmd_args[], unsigned int pmem_size, UserContext *ucont)
{
	char **p;
	int i;

	init_args = &cmd_args[1];
	PMEM_LIMIT = pmem_size;
	pmem_fn = (pmem_size >> PAGESHIFT);


	//printf("kernel stack: %x\n", (int)&i);0xff6c0
	memcpy(&ucont_cpy, ucont, sizeof(UserContext));

	/* init int vector */
	init_trap();

	/* init tty*/
	init_tty();

	/* init kernel page table */
	init_kernel_pgt();

	/* init p frames */
	init_pmem();

	/* init structures of ipc */
	init_ipc();

	/* write the reg for vmem */
	WriteRegister(REG_PTBR0, (uint_32)kernel_pgt);
	WriteRegister(REG_PTLR0, MAX_PT_LEN);

	/* enable the reg vm enable */
	WriteRegister(REG_VM_ENABLE, 1);

	/* init proc, create idle & init proc */
	init_proc();

	/* write the */
	WriteRegister(REG_PTBR1, (uint_32)current->user_pg);
	WriteRegister(REG_PTLR1, MAX_PT_LEN);

	if (LoadProgram(cmd_args[0], init_args, current) == -1) {
		TracePrintf(0, "Load Wrong\n");
		exit(ERROR);
	}

/* TODO	
 * We orignally want to save the kernel stack and kernel context here, 
 * so that when a new program was created, it can run from here
 */
	init_kcont();

	memcpy(ucont, &(current->ucont), sizeof(UserContext));
}


/*
 * Determine whether the new heap request can be satisfied
 */
int SetKernelBrk (void *addr)
{
	uint_32 naddr = (uint_32)addr;
	uint_32 old_pgl = KERNEL_HEAP_PGL;
	int i;


	/* overflow to stack area */
	if (naddr >= (VMEM_0_LIMIT - PAGESIZE * 2)) {
		printf("ERROR when malloc\n");
		return ERROR;
	}

	KERNEL_HEAP_PGL = ((UP_TO_PAGE(naddr)) >> PAGESHIFT);
	for (i = old_pgl; i < KERNEL_HEAP_PGL; i ++) {
		kernel_pgt[i].valid = true;
		kernel_pgt[i].prot = PROT_READ | PROT_WRITE;
	}

	return 0;
}


#include "kernel.h"
#include "resource.h"
#include "proc.h"


uint_32 pids_bitmap[MAX_PID >> 5];
struct rsc_t pids[MAX_PID];
struct rsc_q free_pids;

struct pcb_t *current;




/* 
 * block_q is the queue to which the current to going to wait in,
 * the state defines the current's state (block? wait?)
 */
struct pcb_t *ready_q, *block_q, *zombie_q, *delay_q;


/*
 * queues are a critical ds for pcb
 * So this section of codes provides several queue operations for pcb
 */
bool enqueue(struct pcb_t **head, struct pcb_t *proc)
{
	struct pcb_t *h = *head;
	if (*head == NULL) {
		*head = proc;
		proc->prev = NULL;
		proc->next = NULL;
	}

	else {
		while(h->next != NULL) {
			h = h->next;
		}

		h->next = proc;
		proc->next = NULL;
		proc->prev = h;
	}

	return true;
}

void enqueue_front(struct pcb_t **head, struct pcb_t *proc)
{
	proc->prev = NULL;

	if (*head == NULL)
		proc->next = NULL;
	else {
		(*head)->prev = proc;
		proc->next = *head;
	}
	*head = proc;
}

bool dequeue(struct pcb_t **head, struct pcb_t *proc)
{
	if (*head == NULL)
		return false;
	if (proc->prev == NULL)
		*head = proc->next;
	else 
		proc->prev->next = proc->next;

	if (proc->next != NULL)
		proc->next->prev = proc->prev;

	proc->prev = NULL;
	proc->next = NULL;

	return true;
}

inline void block_current(struct pcb_t **block_q_ptr, uint_8 state) 
{
	enqueue(block_q_ptr, current);
	current->state = state;
}

inline void 
switch_queue(struct pcb_t *proc, struct pcb_t **new_queue_ptr, uint_8 state)
{
	dequeue(&ready_q, proc);
	enqueue(new_queue_ptr, proc);

	proc->state = state;
}

bool init_proc_kstack(struct pcb_t *pcb)
{
	int i ;

	/* free frame counter */
	if (ff_cnt < 2)
		return false;

	for (i = 0; i < 2; i ++) {
		pcb->kstack_pg[i].pfn = alloc_frame(pcb, i - 2);
		pcb->kstack_pg[i].valid = true;
		pcb->kstack_pg[i].prot = PROT_READ | PROT_WRITE;
	}

	return true;
}

inline void rewrite_kstack_pgt(struct pcb_t *proc)
{
	int i;
	for (i = 0; i < 2; i ++)
		memcpy(&kernel_pgt[KERNEL_STACK_PGB + i], &proc->kstack_pg[i], sizeof(struct pte));
	WriteRegister(REG_TLB_FLUSH, KERNEL_STACK_BASE);
	WriteRegister(REG_TLB_FLUSH, KERNEL_STACK_BASE + PAGESIZE);
}

bool init_proc_pgt(struct pcb_t *proc)
{
	int i;

	for (i = 0; i < MAX_PT_LEN; i ++)
		proc->user_pg[i].valid = false;

	/* stack */
	i --;
	proc->user_pg[i].valid = true;
	proc->user_pg[i].prot = PROT_ALL;
	proc->user_pg[i].pfn = alloc_frame(proc, i);
	if (proc->user_pg[i].pfn == NO_FRAME)
		return false;

	for (i = 0; i < 2; i ++)
		memcpy(&(proc->kstack_pg[i]), &kernel_pgt[KERNEL_STACK_PGB + i], sizeof (struct pte));

	/* the first proc */
	if (proc->pid == 0) {
		for (i = KERNEL_STACK_PGB; i < KERNEL_STACK_PGB + 2; i ++)
			if (kernel_pgt[i].valid) {
				frame_add_holder(proc, i - KERNEL_PGN, i);

				memcpy(&(proc->kstack_pg[i-126]), &kernel_pgt[i], sizeof (struct pte));
			}
	}
}


/* create the kernel thread */
void kthread_create(void (*kthread)(void))
{

	int i;
	fid_t fid;
	struct pcb_t *pcb = (struct pcb_t *) malloc (PCB_SIZE);

	pcb->next = NULL;
	pcb->prev = NULL;
	pcb->state = PROC_X;

	/* user context */
	memcpy(&(pcb->ucont), &ucont_cpy, sizeof(UserContext));
	pcb->ucont.pc = (void*)kthread;
	pcb->ucont.sp = (void*)VMEM_1_LIMIT - 4;
	pcb->ucont.ebp = pcb->ucont.sp;
	pcb->ucont.vector = TRAP_CLOCK;

	pcb->pid = alloc_pid();

	/* init page table */
	init_proc_pgt(pcb);

	/* put to the ready queue */
	enqueue(&ready_q, pcb);
}


/*
 *	pid is a kind of resources
 *	The following codes provides procedures of pid
 */
/* set the xth pid's valid bit */
#define set_pid(x)	\
	pids_bitmap[x >> 5] |= (1 << (x & 0x1f))		//right shift means divide 32
/* unset the xth pid's valid bit */
#define unset_pid(x)	\
	pids_bitmap[x >> 5] &= ~(1 << (x & 0x1f))
/* check the xth pid's valid bit */
#define check_pid(x)	\
	pids_bitmap[x >> 5] &= (1 << (x & 0x1f))


int alloc_pid()
{
	struct rsc_t *p = alloc_rsc(&free_pids);

	if (p == NULL)
		return ERROR;

	unset_pid(p->pid);
	return p->pid;
}

void free_pid(int pid)
{
	struct rsc_t *p = &pids[pid];

	/* the pid is free */
	if (check_pid(p->pid))
		return ;

	set_pid(p->pid);
	free_rsc(&free_pids, p);
}



/* init the system thread like idle */
void kthread_idle();
void kthread_init();
extern char **init_args;
void init_proc()
{

	int i;
	struct rsc_t *p;
	struct pcb_t *init_pcb;

	ready_q = NULL, block_q = NULL, zombie_q = NULL, delay_q = NULL;

	/* init pid */	
	for (i = 0; i < MAX_PID-1; i ++) {
		pids[i].next = &pids[i+1];
		pids[i].pid = i;
	}
	pids[i].next = NULL; pids[i].pid = i;

	free_pids.head = &pids[0];
	free_pids.tail = &pids[i];

	/*
	 * create kernel threads.
	 * Let idle be the last procsses, 
	 * since the first job is to clean kcont_cpy and kstack_cpy.
	 */ 
	kthread_create(&kthread_init);
	kthread_create(&kthread_idle);	
	
	current = ready_q;
	dequeue(&ready_q, current);
	current->state = PROC_RUN;
}


/* the page table is released in the exit */
inline void free_pcb(struct pcb_t *proc)
{
	// free(path);
	free(proc);
}


/*
 *	This file is for debugging 
 */



#include "kernel.h"
#include "resource.h"
#include "proc.h"

#ifdef DEBUG	/* open in kernel */
void debug_kernel()
{
	printf("[KERNEL INFO]:\n");
	printf("BSS_BASE: %00000008x\n", KERNEL_BSS_BASE);
	printf("BSS_LIMIT: %00000008x\n, %d\n", KERNEL_BSS_LIMIT, KERNEL_BSS_PGL);
}

void debug_ucont(UserContext *ucont)
{
	printf("[User Context]\n");
	printf("\tpc: %x, sp: %x\n", ucont->pc, ucont->sp);

	printf("\ttrap: %d, addr: %x\n", ucont->vector, ucont->addr);
}


void debug_binary(char *addr, int len)
{
	int i;

	printf("[DEBUG BINADRY]\n");
	for (i = 0; i < len; i ++)
		printf("%00000008x ", addr[i]);

	printf("\n");
}

extern struct rsc_t *frames;
void debug_pg(struct pte *pptr, int i)
{
	struct frame_holder *it;
	if (pptr->valid) {
		printf("\tupage#%d -> frame#%d, prot: %x, [owner: ", i, pptr->pfn, pptr->prot);

		it = frames[pptr->pfn].fm->holder_list;
		while (it != NULL) {
			printf(" (%d, %d), ", it->proc->pid, it->index);
			it = it->next;
		}
		printf("]\n");
	}
}

void debug_upgt(struct pcb_t *proc)
{
	int i;
	printf("[Kstack PageTable]:\n");
	for (i = 0; i < 2; i ++)
		debug_pg(&proc->kstack_pg[i], i);
	printf("[User PageTable]:\n");
	for (i = 0; i < MAX_PT_LEN; i ++)
		debug_pg(&proc->user_pg[i], i);

}

void debug_queue(struct pcb_t *proc)
{
	struct pcb_t *iter = proc;
	while (iter != NULL) {
		printf("Proc#%d(%d)\t", iter->pid, iter->state);
		iter = iter->next;
	}

	printf("\n");
}

#endif

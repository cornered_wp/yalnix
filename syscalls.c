
/* 
 * Syscalls
 */

#include "kernel.h"
#include "proc.h"



/*
 * suppose we let the chld run first,
 * 	so save the kc to parent
 */
KernelContext *__dup_kstate(KernelContext *kc_in, void *parent, void *chld)
{
	char *buf;
	void *kstack_addr = (void*) (VMEM_0_LIMIT - 2 * PAGESIZE);

	memcpy(&TYPE_PCB(parent)->kcont, kc_in, sizeof(KernelContext));
	TYPE_PCB(parent)->state = PROC_READY;
	enqueue(&ready_q, parent);

	buf = (char *) malloc (PAGESIZE * 2);
	memcpy(buf, kstack_addr, PAGESIZE * 2);

	rewrite_kstack_pgt(TYPE_PCB(chld));
	WriteRegister(REG_PTBR1, (uint_32)(TYPE_PCB(chld)->user_pg));
	WriteRegister(REG_TLB_FLUSH, TLB_FLUSH_1);

	memcpy(kstack_addr, buf, PAGESIZE * 2);

	TYPE_PCB(chld)->state = PROC_RUN;
	current = TYPE_PCB(chld);
	return kc_in;
}
#define dup_kstate(parent, chld)	\
	KernelContextSwitch(&__dup_kstate, parent, chld)


/* 
 * 1. prepare the pcb
 * 2. copy the whole stuff from parent to chld 
 * 3. reset pid
 * 4. duplicate the user page table
 * 5. alloc frames for kernel stack
 * 	now this part is little tricky
 * 		if the chld is going to run first (some queue operation also needed)), first copy the kernel stack into a buffer, write the new frame id to kernel_pgt, and flush TLB, then copy the buffer to kstack
 *		if the parent is going to run first, basically the same with more operations
 * 6. the queue operations
 * 7. set the ucont->regs[0] (the return value)
 */
int sys_fork(UserContext *ucont)
{
	struct pcb_t *chld;
	int i;
	int chld_pid = alloc_pid();
	fid_t fid;

	/* no pid */
	if (chld_pid == ERROR)
		return ERROR;

	/* need at least two frames for kernel stack */
	if (ff_cnt < PROC_KERNEL_NPG)
		return ERROR;


	alloc_pcb(chld);
	memcpy(chld, current, PCB_SIZE);
	chld->pid = chld_pid;

	/* the link struct */
	chld->parent = current;
	chld->sibling = current->chld;
	current->chld = chld;

	/* share user space */
	dup_pgt(chld, current);

	/* alloc kstack frames (now the kernel stack must be in use, so don't worry) */
	for (i = 0; i < 2; i ++)
		if (chld->kstack_pg[i].valid)
			chld->kstack_pg[i].pfn = alloc_frame(chld, i - 2);

	/* 
	 * Use KernelContextSwitch to duplicate the KernelContext 
	 * The real point when fork happenes
	 */
	dup_kstate(current, chld);

	if (current->pid == chld_pid)
		return 0;
	else
		return chld_pid;

}


/*
 * Basically same as the LoadProgram
 */
#define ENV 	"user/"
int sys_exec(char* path, char *args[])
{
	char cpath[100];
	if (args == NULL) {
		args = (char**) malloc (sizeof(char*));
		args[0] = NULL;
	}
	current->ucont.regs[0] = LoadProgram(path, args, current);

	if (0 != current->ucont.regs[0]) {
		strcpy(cpath, ENV);
		strcat(cpath, path);
		current->ucont.regs[0] = LoadProgram(cpath, args, current);
	}

	return current->ucont.regs[0];
}

/* 
 * ARG: addr
 * Function: set the heap break to addr
 */
int sys_brk(void *addr)
{
	int i, page_id = (uint_32)addr >> PAGESHIFT;

	printf("brk addr: %x\n", (uint_32)addr);

	page_id -= KERNEL_PGN;

	if (page_id < current->uheap_base)
		return ERROR;
		
	/* to reclaim some space in heap */
	if (page_id < current->uheap_limit)
		for (i = page_id; i < current->uheap_limit; i ++)
			discard_page(current, i);
	/* to aquire some space in heap */
	else {
		if (ff_cnt < page_id - current->uheap_limit)
			return ERROR;

		for (i = current->uheap_limit; i < page_id; i ++)
			alloc_upage(current, i, PROT_READ | PROT_WRITE);
	}

	current->uheap_limit = page_id;
	return 0;
}

/* 
 * when a process call wait, 
 * check whether it has children, if not return 1
 * check whether it has a zombie chld, if yes, return the exit status of that chld
 * else Put it in the block queue, set the status as wait
 */
int sys_wait(int *addr)
{
	struct pcb_t *chld = current->chld;

	/* no chld */
	if (chld == NULL)
		return ERR_NOCHLD;
	
	/* one of the chld is zombie process */
	/* should we free the proc? */
	while (chld != NULL) {
		if (chld->state != PROC_ZOMBIE) 
			chld = chld->sibling;
		else {
			*addr = chld->exit_stat;
			chld->state = PROC_DEAD;
			return 0;
		}
	}
			
	block_current(&block_q, PROC_WAIT);

	sched();
	*addr = current->wait_stat;
	return 0;
}


/*
 * 1.
 */
int sys_exit(int status)
{
	struct pcb_t *parent, *chld, *p;
	int i;

	TracePrintf(0, " Proc#%d exit with code: %d\n", current->pid, status);
	current->exit_stat = status;

	/* 
	 * zombie_q is not really needed, 
	 * just some times system may want to know all the zombies
	 */ 
	block_current(&zombie_q, PROC_ZOMBIE);

	/* if parent is waiting for him */
	parent = current->parent;

	if ( (parent != NULL) && (parent->state == PROC_WAIT) ) {
		parent->wait_stat = status;
		dequeue(&block_q, parent);
		enqueue_ready(parent);
	}

	/* if chld is in zombie state */
	chld = current->chld;
	while (chld != NULL) {
		p = chld->sibling;
		if ((chld->state == PROC_ZOMBIE) || (chld->state == PROC_DEAD)) {
			dequeue(&zombie_q, chld);
			free_pcb(chld);
		}
		else {
			/* should we just kill it? We didn't */
			chld->parent = parent;
			chld->sibling = parent->chld;
			parent->chld = chld;
		}
		chld = p;
	}

	/* 
	 * Release kstack_pg and user_pg.
	 * 	discard_page will handle all the nasty things
	 * 		like frames in swap area
	 */
	for (i = 0; i < 2; i ++)
		discard_page(current, i - 2);

	for (i = 0; i < MAX_PT_LEN; i ++)
		discard_page(current, i);

	sched();
}

inline int get_pid()
{
	return current->pid;
}

void sys_delay(uint_32 units)
{
	set_alarm(units);
	sched();
}


/* give up the processor */
inline void sys_yield()
{
	sched();
}
